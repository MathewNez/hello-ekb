# Generated by Django 4.2.4 on 2023-08-13 07:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('external_id', models.IntegerField()),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=250)),
                ('place', models.CharField(max_length=150)),
                ('dates', models.CharField(max_length=150)),
                ('source', models.CharField(max_length=50)),
            ],
        ),
    ]
