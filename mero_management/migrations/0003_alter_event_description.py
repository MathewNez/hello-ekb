# Generated by Django 4.2.4 on 2023-08-13 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mero_management', '0002_alter_event_dates_alter_event_place_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='description',
            field=models.CharField(max_length=1024),
        ),
    ]
