from enum import Enum


class EventSourceNames(Enum):
    KUDA_GO: str = 'kuda_go'
    EKB_RF: str = 'ekb_rf'
