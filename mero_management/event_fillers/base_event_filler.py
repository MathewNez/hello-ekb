from dataclasses import dataclass
from typing import Type, Optional, List, Dict
from abc import ABC, abstractmethod

from mero_management.constants import EventSourceNames
from mero_management.data_sources.base_datasource import AbstractResourceParser
from mero_management.data_sources.kudaGo import KudaGo
from mero_management.data_transformers.base_transformer import AbstractTransformer
from mero_management.models import Event


@dataclass
class AbstractEventFiller(ABC):
    parser_: AbstractResourceParser
    transformer: AbstractTransformer
    date_from: str
    date_to: Optional[str] = None

    @abstractmethod
    def fill_database(self):
        events: List[Dict] = self.parser_.get_events(self.date_from, self.date_to)
        for event in events:
            self.transformer.set_source(event)
            event_model = self.transformer.to_model()
            exists = Event.objects.filter(
                external_id=event_model.external_id,
                source=EventSourceNames.KUDA_GO.value
            ).exists()
            if not exists:
                event_model.save()
