from datetime import date
from typing import Tuple, List

from django.conf import settings

from mero_management.data_sources.base_datasource import AbstractResourceParser
from mero_management.data_sources.ekbRF import EkbRF
from mero_management.data_sources.kudaGo import KudaGo
from mero_management.data_transformers.EkbRFTransformer import EkbRFTransformer
from mero_management.data_transformers.KudaGoTransformer import KudaGoTransformer
from mero_management.data_transformers.base_transformer import AbstractTransformer
from mero_management.event_fillers.base_event_filler import AbstractEventFiller


def fill_db():
    today: str = date.today().isoformat()
    parsers_n_transformers: List[Tuple[AbstractResourceParser, AbstractTransformer]] = [
        (KudaGo(settings.KUDAGO_API_URL), KudaGoTransformer()),
        (EkbRF(settings.EKB_RF_URL), EkbRFTransformer())
    ]
    for parser, transformer in parsers_n_transformers:
        db_filler = AbstractEventFiller(parser, transformer, today)
        db_filler.fill_database()
