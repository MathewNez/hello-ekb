from django.core.signals import request_started
from django.dispatch import receiver

from mero_management.celery_tasks.fill_db import fill_db


@receiver(request_started, dispatch_uid="run_only_once")
def fill_db_on_startup():
    print('Started parsing data sources')
    fill_db()
    print('Data sources parsed')

