from mero_management.data_transformers.base_transformer import AbstractTransformer
from mero_management.models import Event
from mero_management.constants import EventSourceNames


class EkbRFTransformer(AbstractTransformer):
    def to_model(self) -> Event:
        return Event(
            external_id=self.source_dict.get('ekbrf_id'),
            title=self.source_dict.get('title'),
            description=self.source_dict.get('subtitle'),
            place=self.source_dict.get('site'),
            dates=self.source_dict.get('date'),
            source=EventSourceNames.EKB_RF.value
        )
