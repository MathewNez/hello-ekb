from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Dict, Optional

from mero_management.models import Event


@dataclass
class AbstractTransformer(ABC):
    source_dict: Optional[Dict] = None

    def set_source(self, source_dict: Dict):
        self.source_dict = source_dict

    @abstractmethod
    def to_model(self) -> Event:
        raise NotImplementedError('This method not implemented in parent class')
