from typing import Optional, List, Dict

from mero_management.data_transformers.base_transformer import AbstractTransformer
from mero_management.models import Event
from mero_management.constants import EventSourceNames


class KudaGoTransformer(AbstractTransformer):
    
    def place_to_str(self) -> str:
        place_info: Optional[Dict] = self.source_dict.get('place')
        place_str = f'{place_info.get("title")} {place_info.get("address")}' if place_info else ''
        return place_str

    def dates_to_str(self) -> str:
        dates_str = []
        dates_info: Optional[List[Dict]] = self.source_dict.get('dates')
        if dates_info:
            for date in dates_info:
                date_str = f'c {date.get("start")} по {date.get("end")}'
                dates_str.append(date_str)
            return ', '.join(dates_str)

        return ''
    
    def to_model(self) -> Event:
        return Event(
            external_id=self.source_dict.get('id'),
            title=self.source_dict.get('title'),
            description=self.source_dict.get('description'),
            place=self.place_to_str(),
            dates=self.dates_to_str(),
            source=EventSourceNames.KUDA_GO.value
        )
        
