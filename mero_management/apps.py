from django.apps import AppConfig


class MeroManagementConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mero_management'
