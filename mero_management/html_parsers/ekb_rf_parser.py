from bs4 import BeautifulSoup


class EkbRFParser:
    html: str
    soup: BeautifulSoup

    def __init__(self, html: str):
        self.html = html
        self.soup = BeautifulSoup(self.html, 'html.parser')

    def get_total_records(self) -> int:
        return int(self.soup.find(class_='event_dynamic_search_form')['data-total'])

    def parse_events(self):
        parsed_events = []
        events = self.soup.find_all(class_='event_post_item')
        for event in events:
            parsed_event = {'ekbrf_id': int(event['data-gen_id'])}
            keys = ['event_post_item_info_title', 'event_post_item_info_subtitle',
                    'event_post_item_info_date', 'event_post_item_info_site']
            for key in keys:
                try:
                    parsed_event[key.split('_')[-1]] = event.find(class_=key).text
                except AttributeError:
                    print(f'Could not get info of {key} during parsing ekbRF event with id {parsed_event["ekbrf_id"]}')
            parsed_events.append(parsed_event)

        return parsed_events
