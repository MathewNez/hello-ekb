from typing import Dict, List

from mero_management.data_sources.base_datasource import AbstractResourceParser, AbstractResource

import requests


class KudaGoResource(AbstractResource):
    def send_request(self, path: str, params: Dict) -> Dict:
        response = requests.get(f'{self.url}{path}', params=params)
        response_json = response.json()
        return response_json


class KudaGo(AbstractResourceParser, KudaGoResource):
    default_location: str = 'ekb'
    fields: List[str] = ['id', 'title', 'place', 'description', 'dates']
    expand: List[str] = ['place']

    def get_events(self, date_from: str, date_to: str = None):
        all_events: List[Dict] = []
        params = {
            'location': self.default_location,
            'actual_since': date_from,
            'fields': ','.join(self.fields),
            'expand': ','.join(self.expand)
        }
        if date_to:
            params.update({'actual_until': date_to})
        response_json = self.send_request('/events', params)
        while True:
            all_events.extend(response_json.get('results'))
            next_page = response_json.get('next')
            if not next_page:
                break
            response = requests.get(next_page)
            response_json = response.json()

        return all_events
