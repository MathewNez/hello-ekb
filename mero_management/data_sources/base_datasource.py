from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Union, Dict, List


@dataclass
class AbstractResource(ABC):
    url: str

    @abstractmethod
    def send_request(self, path: str, params: Dict) -> Union[Dict, str]:
        raise NotImplementedError('This method not implemented in parent class')


class AbstractResourceParser(AbstractResource):
    @abstractmethod
    def get_events(self, date_from: str, date_to: str = None) -> List[Dict]:
        raise NotImplementedError('This method not implemented in parent class')
