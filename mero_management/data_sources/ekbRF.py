from math import ceil
from typing import List, Dict

import requests

from mero_management.data_sources.base_datasource import AbstractResource, AbstractResourceParser
from mero_management.html_parsers.ekb_rf_parser import EkbRFParser


class EkbRFResource(AbstractResource):
    def send_request(self, path: str, form_data=None) -> str:
        if form_data is None:
            form_data = {}
        response = requests.post(f'{self.url}{path}', data=form_data, verify=False)
        return response.text


class EkbRF(AbstractResourceParser, EkbRFResource):
    default_form_data_prefix = 'archive_filters'
    default_pagination_size = 3

    def get_events(self, date_from: str, date_to: str = None) -> List[Dict]:
        all_events = []
        form_data = {
            f'{self.default_form_data_prefix}[start_date]': date_from,
        }
        if date_to:
            form_data.update(
                {
                    f'{self.default_form_data_prefix}[end_date]': date_to
                }
            )
        url_path = '/events/dynamic_search/'
        html = self.send_request(f'{url_path}0/0', form_data)
        parser_ = EkbRFParser(html)
        pages_count = ceil(parser_.get_total_records()/self.default_pagination_size)
        for i in range(1, pages_count + 1):
            html = self.send_request(f'{url_path}1/{i}', form_data)
            parser_ = EkbRFParser(html)
            all_events.extend(parser_.parse_events())

        return all_events
