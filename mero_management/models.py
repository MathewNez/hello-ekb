from django.db import models


class Event(models.Model):
    external_id = models.IntegerField()
    title = models.CharField(max_length=2048, null=True)
    description = models.CharField(max_length=2048, null=True)
    place = models.CharField(max_length=2048, null=True)
    dates = models.CharField(max_length=2048, null=True)
    source = models.CharField(max_length=2048, null=True)
